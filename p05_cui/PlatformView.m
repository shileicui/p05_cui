//
//  PlatformView.m
//  p05_cui
//
//  Created by SHILEI CUI on 3/26/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlatformView.h"

@implementation PlatformView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        frame.size.width = arc4random()%(int)(screenWidth-frame.origin.x-50)+30;
        self.frame = frame;
    }
    return self;
}

@end
