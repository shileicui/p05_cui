//
//  ViewController.m
//  p05_cui
//
//  Created by SHILEI CUI on 3/26/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "ViewController.h"
#import "PlayerView.h"
#import "PlatformView.h"
#import "TouchView.h"

#define MainScreenHeight           [[UIScreen mainScreen] bounds].size.height
#define MainScreenWidth            [[UIScreen mainScreen] bounds].size.width
#define PlayerViewHeight       20.0
#define PlayerViewWidth        10.0
#define PlayerViewInitialX     80.0
#define PlayerViewInitialY     (MainScreenHeight*2/3)
#define TouchViewX             (PlayerViewInitialX + PlayerViewWidth)
#define TouchViewY             (PlayerViewInitialY + PlayerViewHeight)
#define BottomViewY                TouchViewY
#define BottomViewHeight           (MainScreenHeight - BottomViewY)


@interface ViewController ()<UIAlertViewDelegate>
{
    NSTimer *growTimer;
    TouchView *tView;
    UIView *firstBottomView;
    PlayerView *Player;
    PlatformView *randomBottomView;
    UIView *foreView;
    UIView *nextView;
    NSInteger count;
    UILabel *countL;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initSubViews];
}

#pragma mark - Init Views

- (void)initSubViews
{
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview) withObject:nil];
     count = 0;
     countL = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, MainScreenWidth, 30)];
     countL.textColor = [UIColor whiteColor];
     countL.layer.cornerRadius = 5.0f;
     countL.layer.masksToBounds = YES;
     countL.textAlignment = NSTextAlignmentCenter;
     countL.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
     countL.text = [NSString stringWithFormat:@"%ld",count];
     [self.view addSubview:countL];
    
     UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, MainScreenWidth, 80)];
     l.backgroundColor = [UIColor clearColor];
     l.textColor = [UIColor blackColor];
     l.numberOfLines = 0;
     l.textAlignment = NSTextAlignmentCenter;
     l.text = @"Click the screen and hold to make bridge longer enough to cross the gap.";
     [self.view addSubview:l];
    
    [self initPlayerView];
    [self initFirstBottomView];
    [self initTouchView];
    foreView = firstBottomView;
    nextView = randomBottomView;
}

- (void)initPlayerView
{
    Player = [[PlayerView alloc] initWithFrame:CGRectMake(PlayerViewInitialX, PlayerViewInitialY, PlayerViewWidth, PlayerViewHeight)];
    Player.backgroundColor = [UIColor blackColor];
    [self.view addSubview:Player];
}

- (void)initFirstBottomView
{
    firstBottomView = [[UIView alloc]initWithFrame:CGRectMake(0, BottomViewY, PlayerViewInitialX + PlayerViewWidth,  BottomViewHeight)];
    firstBottomView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:firstBottomView];
    
    randomBottomView = [[PlatformView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(firstBottomView.frame)+100, BottomViewY, 70, BottomViewHeight)];
    randomBottomView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:randomBottomView];
}


- (void)initTouchView
{
    CGRect touchViewRect = CGRectMake(TouchViewX, TouchViewY, 5, 0);
    if (tView)
    {
        [tView removeFromSuperview];
    }
    tView = [[TouchView alloc]initWithFrame:touchViewRect rate:1];
    [self.view addSubview:tView];
}

- (void)callTimer
 {
     growTimer = [NSTimer timerWithTimeInterval:0.01 target:tView selector:@selector(growHeight) userInfo:nil repeats:YES];
     [[NSRunLoop currentRunLoop] addTimer:growTimer forMode:NSDefaultRunLoopMode];
     [growTimer fire];
 }
 

 - (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
 {
     [self initTouchView];
     [self callTimer];
 }
 
 
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [growTimer invalidate];
    [tView endGrowAfter:^(float length) {
        BOOL isDrop = NO;
        if (length >= CGRectGetMinX(nextView.frame) - CGRectGetMaxX(foreView.frame) && length <= CGRectGetMaxX(nextView.frame) - CGRectGetMaxX(foreView.frame))
        {
            length = CGRectGetMaxX(nextView.frame) - CGRectGetMaxX(foreView.frame);
            count++;
            countL.text = [NSString stringWithFormat:@"%ld",count];
        }else{
            isDrop = YES;
        }
        [Player run:length after:^{
            if (isDrop)
            {
                [tView dropAfter:^(float length) {
                }];
                [Player dropAfter:^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lost" message:@"Game Over！" delegate:self cancelButtonTitle:@"Play again:)" otherButtonTitles:nil, nil];
                    [alert show];
                }];
            }else{
                CGRect foreRect = foreView.frame;
                CGRect nextRect = nextView.frame;
                CGRect playerRect = Player.frame;
                CGRect touchGrowRect = tView.frame;
                
                CGFloat distance = playerRect.origin.x - PlayerViewInitialX;
                PlatformView *randomView = [[PlatformView alloc] initWithFrame:CGRectMake(arc4random_uniform((int)(MainScreenWidth - PlayerViewInitialX - PlayerViewWidth- 80))+PlayerViewInitialX+PlayerViewWidth+30, foreView.frame.origin.y, 10, foreView.frame.size.height)];
                randomView.backgroundColor = [UIColor blackColor];
                CGRect randomRect = randomView.frame;
                CGFloat tempX = randomRect.origin.x;
                randomRect.origin.x = MainScreenWidth;
                randomView.frame = randomRect;
                [self.view addSubview:randomView];
                
                foreRect.origin.x -= distance;
                nextRect.origin.x -= distance;
                playerRect.origin.x -= distance;
                touchGrowRect.origin.x -= distance;
                touchGrowRect.origin.y -= 5;
                randomRect.origin.x = tempX;
                [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    foreView.frame = foreRect;
                    nextView.frame = nextRect;
                    Player.frame = playerRect;
                    tView.frame = touchGrowRect;
                    randomView.frame = randomRect;
                } completion:^(BOOL finished) {
                    [foreView removeFromSuperview];
                    foreView = nextView;
                    nextView = randomView;
                }];
            }
        }];
    }];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self initSubViews];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
