//
//  PlayerView.m
//  p05_cui
//
//  Created by SHILEI CUI on 3/26/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerView.h"

static NSString *runAnimationKey = @"runPozitionAnimation";
static NSString *dropAnimationKey = @"dropPozitionAnimation";

@interface PlayerView()
{
    CGPoint originPoint;
}
@end

@implementation PlayerView

- (void)run:(float)distance after:(dispatch_block_t)result
 {
     originPoint = self.frame.origin;
     [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
         self.frame = CGRectMake(originPoint.x + distance, originPoint.y, self.frame.size.width, self.frame.size.height);
     } completion:^(BOOL finished){
        result();
     }];
 
 }
 
 
 - (void)dropAfter:(void(^)(void))endGrowHandler
 {
     CGRect rect = self.frame;
     rect.origin.y = [UIScreen mainScreen].bounds.size.height;
     [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
         self.frame = rect;
     } completion:^(BOOL finished) {
         endGrowHandler();
     }];
 }
 
 - (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
 {
     if (flag)
     {
         if ([anim isEqual:[self.layer animationForKey:runAnimationKey]])
         {
         }
     }
     
 }

@end
